﻿using System;
using System.Security.Cryptography;

namespace EnigmaProject.Classes.Cryptography
{
    /// <summary>
    /// Simplified class designed to handle two basic hash operations:
    /// 1) generating a hash from an input string;
    /// 2) verifying a new input against an existing hash string.
    /// Based on <see cref="https://stackoverflow.com/a/10402129"/>.
    /// </summary>
    public static class PasswordHash
    {
        /// <summary>
        /// Number of iterations used in <see cref="Rfc2898DeriveBytes(string, byte[], int)"> to generate a hash. Manually defined here while salt is being randomized in <see cref="GetHash(string)"/>.
        /// </summary>
        private const int HashGenIterations = 10000;

        /// <summary>
        /// Generates a hash based on input string, randomized salt and previously defined iterations number, using <see cref="System.Security.Cryptography"/> types.
        /// </summary>
        /// <param name="input">Input string.</param>
        /// <returns>Hash.</returns>
        public static string GetHash(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                throw new ArgumentException("Input string cannot be null or empty.", nameof(input));
            }

            byte[] salt = new byte[16];
            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(salt);
            }

            byte[] hash = new byte[20];
            using (var pbkdf2 = new Rfc2898DeriveBytes(input, salt, HashGenIterations))
            {
                hash = pbkdf2.GetBytes(20);
            }

            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            return Convert.ToBase64String(hashBytes);
        }

        /// <summary>
        /// Verifies the entered input against a stored hash.
        /// </summary>
        /// <param name="hash">Hash string.</param>
        /// <param name="input">Input string.</param>
        /// <returns>True if the hash corresponds the input.</returns>
        public static bool Verify(string hash, string input)
        {
            if (string.IsNullOrEmpty(hash))
            {
                throw new ArgumentException("Hash string cannot be null or empty.", nameof(hash));
            }

            if (string.IsNullOrEmpty(input))
            {
                throw new ArgumentException("Input string cannot be null or empty.", nameof(input));
            }

            byte[] hashBytes = Convert.FromBase64String(hash);

            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);

            byte[] thisHash = new byte[20];
            using (var pbkdf2 = new Rfc2898DeriveBytes(input, salt, HashGenIterations))
            {
                thisHash = pbkdf2.GetBytes(20);
            }

            for (int i = 0; i < 20; i++)
            {
                if (hashBytes[i + 16] != thisHash[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}
