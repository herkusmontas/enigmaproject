﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EnigmaProject.Classes.Cryptography
{
    /// <summary>
    /// Contains methods to encrypt/decrypt a string using Caesar Cipher, shifting symbols using alphabets collection.
    /// </summary>
    public static class CaesarCipher
    {
        /// <summary>
        /// A list of alphabets (a set of symbols to shift within). Contains latin, cyrillic alphabets, digits and basic punctuation by default. Can be expanded during a runtime via <see cref="AlphabetAdd(string)"/> method.
        /// </summary>
        private static List<string> alphabets = new List<string>()
        {
            { "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ" },
            { "ABCDEFGHIJKLMNOPQRSTUVWXYZ" },
            { "0123456789" },
            { " !.,?:;\\\"\'[]{}()" },
        };

        /// <summary>
        /// Encrypts a string message, shifting it's every symbol within it's alphabet.
        /// </summary>
        /// <param name="message">Input string message.</param>
        /// <param name="key">The shift parameter.</param>
        /// <returns>Encrypted message.</returns>
        public static string Encrypt(string message, int key)
        {
            StringBuilder text = new StringBuilder(message);

            for (var i = 0; i < text.Length; i++)
            {
                bool isLower = char.IsLower(text[i]);
                string alphabet = string.Empty;

                text[i] = char.ToUpperInvariant(text[i]);

                foreach (string item in alphabets)
                {
                    if (item.Contains(text[i]))
                    {
                        alphabet = item;
                        break;
                    }
                }

                if (string.IsNullOrEmpty(alphabet))
                {
                    continue;
                }

                int shiftedIndex = alphabet.IndexOf(text[i]) + key;
                while (shiftedIndex < 0)
                {
                    shiftedIndex = alphabet.Length + shiftedIndex;
                }

                shiftedIndex %= alphabet.Length;

                text[i] = isLower ? char.ToLowerInvariant(alphabet[shiftedIndex]) : alphabet[shiftedIndex];
            }

            return text.ToString();
        }

        /// <summary>
        /// Decrypts a string message.
        /// </summary>
        /// <param name="message">Encoded string message.</param>
        /// <param name="key">The shift parameter.</param>
        /// <returns>Decoded message.</returns>
        public static string Decrypt(string message, int key)
        {
            return Encrypt(message, -key);
        }

        /// <summary>
        /// Encrypts a string message, shifting it's every symbol within it's alphabet.
        /// </summary>
        /// <param name="message">Input string message.</param>
        /// <param name="key">The shift parameter.</param>
        public static void Encrypt(ref string message, int key)
        {
            message = Encrypt(message, key);
        }

        /// <summary>
        /// Decrypts a string message.
        /// </summary>
        /// <param name="message">Encoded string message.</param>
        /// <param name="key">The shift parameter.</param>
        public static void Decrypt(ref string message, int key)
        {
            message = Decrypt(message, key);
        }

        /// <summary>
        /// Add a custom set of symbols to the alphabets list.
        /// </summary>
        /// <param name="alphabet">A set of symbols.</param>
        public static void AlphabetAdd(string alphabet)
        {
            if(string.IsNullOrWhiteSpace(alphabet))
			{
                throw new ArgumentException("Input alphabet cannot be empty, null or whitespace.", nameof(alphabet));
			}

            alphabets.Add(alphabet);
        }
    }
}
