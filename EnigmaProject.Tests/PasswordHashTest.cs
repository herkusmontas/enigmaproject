﻿using EnigmaProject.Classes.Cryptography;
using NUnit.Framework;
using System;

namespace EnigmaProject.Tests
{
	[TestFixture]
	public class PasswordHashTest
	{
		[TestCase("test", "test")]
		[TestCase("test", "aaaa")]
		public void GetHash_SaltMatters(string inputOne, string inputTwo)
		{
			Assert.AreNotEqual(PasswordHash.GetHash(inputOne), PasswordHash.GetHash(inputTwo));
		}

		[TestCase("test", "test", ExpectedResult = true)]
		[TestCase("test", "aaaa", ExpectedResult = false)]
		public bool Verify_WorksAsExpected(string stored, string input)
		{
			string hash = PasswordHash.GetHash(stored);

			return PasswordHash.Verify(hash, input);
		}

		[Test]
		public void GetHash_InputIsEmpty_ThrowArgumentException() =>
			Assert.Throws<ArgumentException>(() => PasswordHash.GetHash(string.Empty), message: "Input string cannot be null or empty.");

		[Test]
		public void GetHash_InputIsNull_ThrowArgumentException() =>
			Assert.Throws<ArgumentException>(() => PasswordHash.GetHash(null), message: "Input string cannot be null or empty.");

		[Test]
		public void Verify_HashIsNull_ThrowArgumentException() =>
			Assert.Throws<ArgumentException>(() => PasswordHash.Verify(null, "test"), message: "Hash string cannot be null or empty.");

		[Test]
		public void Verify_HashIsEmpty_ThrowArgumentException() =>
			Assert.Throws<ArgumentException>(() => PasswordHash.Verify(string.Empty, "test"), message: "Hash string cannot be null or empty.");

		[Test]
		public void Verify_InputIsEmpty_ThrowArgumentException() =>
			Assert.Throws<ArgumentException>(() => PasswordHash.Verify("test", string.Empty), message: "Input string cannot be null or empty.");

		[Test]
		public void Verify_InputIsNull_ThrowArgumentException() =>
			Assert.Throws<ArgumentException>(() => PasswordHash.Verify("test", null), message: "Input string cannot be null or empty.");
	}
}
