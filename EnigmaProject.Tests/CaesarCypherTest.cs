﻿using EnigmaProject.Classes.Cryptography;
using NUnit.Framework;
using System;

namespace EnigmaProject.Tests
{
	[TestFixture]
	public class CaesarCypherTest
	{
		[TestCase("Чу, я слышу пушек гром! dCode Caesar", 0, ExpectedResult = "Чу, я слышу пушек гром! dCode Caesar")]
		[TestCase("Чу, я слышу пушек гром! dCode Caesar", 1, ExpectedResult = "Шф?!а!тмьщф!рфщёл!дспн.!eDpef!Dbftbs")]
		[TestCase("Чу, я слышу пушек гром! dCode Caesar", 15, ExpectedResult = "Ёв.)н)аъйжв)ювжущ)сяэы )sRdst)Rpthpg")]
		[TestCase("Чу, я слышу пушек гром! dCode Caesar", 33, ExpectedResult = "Чу?!я!слышу!пушек!гром.!kJvkl!Jhlzhy")]
		[TestCase("Чу, я слышу пушек гром! dCode Caesar", 34, ExpectedResult = "Шф:.а.тмьщф.рфщёл.дспн,.lKwlm.Kimaiz")]
		[TestCase("Чу, я слышу пушек гром! dCode Caesar", 67, ExpectedResult = "Шф;,а,тмьщф,рфщёл,дспн?,sRdst,Rpthpg")]
		[TestCase("АбAb", -1, ExpectedResult = "ЯаZa")]
		[TestCase("АбAb", -34, ExpectedResult = "ЯаSt")]
		[TestCase(null, 1, ExpectedResult = "")]
		[TestCase("", 1, ExpectedResult = "")]
		public string Encrypt_WorksAsExpected(string message, int key)
		{
			return CaesarCipher.Encrypt(message, key);
		}
		
		[TestCase("Чу, я слышу пушек гром! dCode Caesar", 0, ExpectedResult = "Чу, я слышу пушек гром! dCode Caesar")]
		[TestCase("Шф?!а!тмьщф!рфщёл!дспн.!eDpef!Dbftbs", 1, ExpectedResult = "Чу, я слышу пушек гром! dCode Caesar")]
		[TestCase("Ёв.)н)аъйжв)ювжущ)сяэы )sRdst)Rpthpg", 15, ExpectedResult = "Чу, я слышу пушек гром! dCode Caesar")]
		[TestCase("Чу?!я!слышу!пушек!гром.!kJvkl!Jhlzhy", 33, ExpectedResult = "Чу, я слышу пушек гром! dCode Caesar")]
		[TestCase("Шф:.а.тмьщф.рфщёл.дспн,.lKwlm.Kimaiz", 34, ExpectedResult = "Чу, я слышу пушек гром! dCode Caesar")]
		[TestCase("Шф;,а,тмьщф,рфщёл,дспн?,sRdst,Rpthpg", 67, ExpectedResult = "Чу, я слышу пушек гром! dCode Caesar")]
		[TestCase("ЯаZa", -1, ExpectedResult = "АбAb")]
		[TestCase("ЯаSt", -34, ExpectedResult = "АбAb")]
		[TestCase(null, 1, ExpectedResult = "")]
		[TestCase("", 1, ExpectedResult = "")]
		public string Decrypt_WorksAsExpected(string message, int key)
		{
			return CaesarCipher.Decrypt(message, key);
		}

		[Test]
		public void AlphabetAdd_WorksAsExpected()
		{
			string text = "A1$";

			Assert.AreEqual(CaesarCipher.Encrypt(text, 1), "B2$");

			CaesarCipher.AlphabetAdd("@$^");

			Assert.AreEqual(CaesarCipher.Encrypt(text, 1), "B2^");
		}

		[Test]
		public void AlphabetAdd_AlphabetIsNull_ThrowArgumentException() =>
			Assert.Throws<ArgumentException>(() => CaesarCipher.AlphabetAdd(null), message: "Input alphabet cannot be empty, null or whitespace.");
		
		[Test]
		public void AlphabetAdd_AlphabetIsEmpty_ThrowArgumentException() =>
			Assert.Throws<ArgumentException>(() => CaesarCipher.AlphabetAdd(string.Empty), message: "Input alphabet cannot be empty, null or whitespace.");
		
		[Test]
		public void AlphabetAdd_AlphabetIsWhiteSpace_ThrowArgumentException() =>
			Assert.Throws<ArgumentException>(() => CaesarCipher.AlphabetAdd("    "), message: "Input alphabet cannot be empty, null or whitespace.");
	}
}
